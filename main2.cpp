#include <cstring>
#include <cstdio>


using namespace std;

char **split(char *input, char const *separator) {
    char *tok = strtok(input, separator);
    while (tok != nullptr) {
        printf("%s\n", tok);
        tok = strtok(nullptr, separator);
    }
}

int main() {
    char buf[128];
    FILE *fp = fopen("test.tsv", "r");

    if (fp == nullptr) {
        perror("Error opening file");
        return 1;
    }

    while (fgets(buf, 128, fp) != nullptr) {
        split(buf, "\t");
    }

    fclose(fp);

    return 0;
}
