#ifndef KOATUU_KDOCUMENT_H
#define KOATUU_KDOCUMENT_H

#include <cstring>
#include <vector>

#include <unordered_map>

#include "../strutil.h"

#include "KEntry.h"

using namespace std;

typedef unordered_map<string, KEntry*> KIndex;

class KDocument {
public:
    KIndex index;

    explicit KDocument(const string &filename = nullptr);

    bool fromFile(const string &filename);
    vector<KEntry*> diff(KDocument *document);
};

#endif //KOATUU_KDOCUMENT_H
