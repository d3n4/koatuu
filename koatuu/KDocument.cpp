#include "KDocument.h"

KDocument::KDocument(const string &filename) {
    if(!filename.empty()) {
        this->fromFile(filename);
    }
}

bool KDocument::fromFile(const string &filename) {
    string line;
//    string fn(filename);
    ifstream fp(filename);

    if (!fp.is_open()) {
        return false;
    }

    while (getline(fp, line)) {
        string sep = "\t";
        vector<string> f = split(line, sep);
        if (!f.empty() && !f[0].empty() && f[0].length() >= 9) {
            if (f[0].length() < 10) {
                f[0] = "0" + f[0];
            }
            this->index[f[0]] = new KEntry(f[0]);
        }
    }

    fp.close();

    return true;
}

vector<KEntry *> KDocument::diff(KDocument *document) {
    vector<KEntry *> diff;
    printf("------ SEARCHING\n");

    for (auto &e: this->index) {
        if(!document->index[e.first]) {
            printf("FOUND MISSING %s\n", e.second->koatuu.c_str());
            diff.push_back(e.second);
        }
    }

    return diff;
}


