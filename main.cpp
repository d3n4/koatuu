#include <cstring>
#include <cstdio>
#include <fstream>
#include <vector>
#include <unordered_map>

#include "koatuu/KDocument.h"
#include "strutil.h"


using namespace std;


unordered_map<string, vector<string>> koatuu_map_file(string &filename) {
    unordered_map<string, vector<string>> idx;

    string sep = "\t";

    string line;

    ifstream fp(filename);

    if (fp.is_open()) {
        while (getline(fp, line)) {
            vector<string> f = split(line, sep);
            if (!f.empty() && !f[0].empty() && f[0].length() >= 9) {
                if (f[0].length() < 10) {
                    f[0] = "0" + f[0];
                }
                idx[f[0]] = f;
            }
        }

        fp.close();
    }

    return idx;
}

int main() {
    auto *docA = new KDocument("testA.tsv");
    auto *docB = new KDocument("testB.tsv");

//    docA->fromFile("testA.tsv");
//    docB->fromFile("testB.tsv");

    docA->diff(docB);
    docB->diff(docA);
//    char out[11];
//    sprintf(out, "%010d", stoi("1231255"));
//    printf("[%s]\n", out);
//
//    string strA = "testA.tsv";
//    string strB = "testB.tsv";
//
//    auto idxA = koatuu_map_file(strA);
//    auto idxB = koatuu_map_file(strB);
//
//    printf("A: %s\n", idxA["0120480803"][2].c_str());
//    printf("B: %s\n", idxB["0120480803"][2].c_str());
//
//    vector<string> uAB;
//    vector<string> uBA;
//
//    for (auto &kv: idxA) {
//        vector<string> &tmp = idxB[kv.first];
//        if (tmp.empty()) {
////            printf("- %s\n", kv.first.c_str());
//            uAB.push_back(kv.first);
//        } else {
////            printf("+ %s\n", kv.first.c_str());
//        }
//    }
//
//    for (auto &kv: idxB) {
//        vector<string> &tmp = idxA[kv.first];
//        if (tmp.empty()) {
////            printf("- %s\n", kv.first.c_str());
//            uBA.push_back(kv.first);
//        } else {
////            printf("+ %s\n", kv.first.c_str());
//        }
//    }
//
//    printf("uAB %lu\n", uAB.size());
//    printf("uBA %lu\n", uBA.size());

    return 0;
}
