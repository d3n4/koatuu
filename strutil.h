#ifndef __STRUTIL__
#define __STRUTIL__

#include <cstring>
#include <cstdarg>
#include <cstdio>
#include <fstream>
#include <vector>
#include <unordered_map>

using namespace std;

vector<string> split(string &s, string &delimiter);

#endif