#include <cstring>
#include <cstdio>
#include <iostream>

using namespace std;

char **split(char *input, const char *separator) {
    char *tok = strtok(input, separator);
    while (tok != nullptr) {
        printf("%s\n", tok);
        tok = strtok(nullptr, separator);
    }
}

int strpos(char *haystack, const char *needle) {
    char *p = strstr(haystack, needle);
    if (!p) return -1;
    return (int) (p - haystack);
}

/*char * pch;
  pch = strstr (str,"simple");
  if (pch != NULL)
    strncpy (pch,"sample",6);*/
void hex(char* str) {
    for (int i = 0; i < strlen(str); i++) {
        if(str[i] != '\0')
        printf("%c ", str[i]);
    }
    printf("\n");
    for (int i = 0; i < strlen(str); i++) {
        printf("%02x ", (uint8_t)str[i]);
    }
    printf("\n");
    for (int i = 0; i < strlen(str); i++) {
        printf("%02d ", i);
    }
    printf("\n");
}

int main() {
    char **buffer;
    FILE *fp = fopen("test.tsv", "r");

    if (fp == nullptr) {
        perror("Error opening file");
        return 1;
    }

    char buf[128];
    while (fgets(buf, 128, fp) != nullptr) {
//        char subbuff[5];
//        memcpy( subbuff, &buff[10], 4 );

//        strcpy(buffer, )s
//        char *p = strstr(buf, "\n");
        int pos = strpos(buf, "\n");
        printf("POS: %d\n", pos);
        hex(buf);
        if(pos < 0) continue;
//        int len = 128 - pos;
        char subbuff[pos];
//        subbuff[pos + 1] = '\0';
        memcpy(subbuff, buf, pos);
//        char subbuff[*p];
        printf("%d %s\n", pos, subbuff);
//        split(buf, "\t");
    }

    fclose(fp);

    return 0;
}
