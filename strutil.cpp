#include "strutil.h"

vector<string> split(string &s, string &delimiter) {
    size_t pos_start = 0, pos_end, delimiterLen = delimiter.length();
    string token;
    vector<string> res;
    while ((pos_end = s.find(delimiter, pos_start)) != string::npos) {
        token = s.substr(pos_start, pos_end - pos_start);
        pos_start = pos_end + delimiterLen;
        res.push_back(token);
    }
    res.push_back(s.substr(pos_start));
    return res;
}
